#Treina uma rede neural que recebe um vetor de 50 elementos entre 0.0 e 1.0
#	e tem como output dois números (t,f) entre 0.0 e 1.0, que devem ser
#	- t=1.0, f=0.0  se  o primeiro elemento do vetor input for maior que 0.5
#	- t=0.0, f=1.0  se  o primeiro elemento do vetor input for menor que 0.5

using NeuralNetworks
using JLD

#Parâmetros
layerSizes = [50, 16, 16, 2]

#Lê dados de treino
trainingData = load("data/firstElem_trainingData.jld", "data")

#Cria a rede
net = createNetwork(layerSizes)

#Treina a rede
for i in 1:500
	trainNetwork(net, trainingData)
end

#Lê dados de teste
testData = load("data/firstElem_testData.jld", "data")

#Testa a rede
testNetwork(net, testData)
