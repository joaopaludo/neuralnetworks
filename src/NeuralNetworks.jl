module NeuralNetworks

export createNetwork
export trainNetwork
export testNetwork
export predict

#---------- Struct de Dados ----------
#Contém as informações básicas da rede
mutable struct Network
	layerSizes::Array{Int, 1}
	numLayers::Int
	weightsSizes::Array{Tuple{Int, Int}, 1}
	
	weights::Array{Array{Float64,2}, 1}
	biases::Array{Array{Float64,1}, 1}
	z::Array{Array{Float64,1}, 1}
end

#Contém as correções que serão aplicadas na rede no treino
mutable struct Corrections
	weights::Array{Array{Float64,2}, 1}
	biases::Array{Array{Float64,1}, 1}
end
#-------------------------------------


#---------- Função de Ativação ----------
#Activation function to introduce non-linearity
activation(x) = 1 / (1 + exp(-x))

#Derivative of the activation function
activation_deriv(x) = exp(-x) / (1 + exp(-x))^2
#----------------------------------------


#---------- Funções usadas no Treinamento ----------
function trainingPredict(net::Network, inputData::Array{Float64, 1})
	#Error checking
	if length(inputData) != net.layerSizes[1]
		println("Error! Number of input data should match the first layer size $(net.layerSizes[1]).")
		return
	end
	
	net.z = [zeros(net.layerSizes[i]) for i in 1:net.numLayers]
	
	prediction = inputData
	net.z[1]  = inputData
	
	#Passes through all layers
	for i in 1:net.numLayers-1
		net.z[i+1] = net.weights[i] * prediction  +  net.biases[i];
		prediction = activation.(net.z[i+1])
	end
		
	return prediction
end

function applyGradient(net::Network, corrections::Corrections, idx_layer::Int, desiredCorrections::Array{Float64,1})
	z = net.z[idx_layer+1]						#Valor z de cada neuronio da camada alvo
	prevNeurons = activation.( net.z[idx_layer] )	#Valor da ativação de cada neuronio da camada anterior
	desiCorrPrev = zeros(net.layerSizes[idx_layer])	#Guarda correções desejadas para os neurônios da camada anterior

	#Pra cada neurônio na camada alvo
	for idx_neuronAtual in 1:net.layerSizes[idx_layer+1]
		#Calcula derivada da função ativação aplicada em z do neuronio atual
		deriv = activation_deriv(z[idx_neuronAtual])
		
		#Calcula e soma mudança desejada nos biases
		corrections.biases[idx_layer][idx_neuronAtual] += 2 * deriv * desiredCorrections[idx_neuronAtual];

		#Calcula as correções nos pesos das ligaçãos entre os neurônios das camadas anterior e o da atual
		corrections.weights[idx_layer][idx_neuronAtual, :] += 2 * deriv * prevNeurons * desiredCorrections[idx_neuronAtual]
		
		#Calcula a mudanças desejadas nos neurônios da camada anterior
		desiCorrPrev += 2 * deriv * net.weights[idx_layer][idx_neuronAtual, :] * desiredCorrections[idx_neuronAtual]
		
		
		#= Código faz a mesma coisa que acima só que num loop
		#E para cada um dos neurônios da layer anterior
		for idx_neuronAnterior in 1:net.layerSizes[idx_layer]
			desiCorrPrev[idx_neuronAnterior] = deriv * net.weights[idx_layer][idx_neuronAtual, idx_neuronAnterior] * desiredCorrections[idx_neuronAtual]
			
			#Calcula a mudança desejada no neurônio da camada anterior
			corrections.neuron[idx_layer][idx_neuronAnterior] += desCorrPre[idx_neuronAnterior]
			
			#Calcula a correção no peso da ligação entre os neurônios das camadas anterior e atual
			corrections.weights[idx_layer][idx_neuronAtual, idx_neuronAnterior] += deriv * neuron[idx_neuronAnterior] * desiredCorrections[idx_neuronAtual]
		end
		=#
	end

	return desiCorrPrev
end

function trainingStep(net::Network, trainingData::Array{ Tuple{Array{Float64,1}, Array{Float64,1}} , 1})
	#Inicialização de Variáveis
	numExemplos = 0
	corrections = Corrections([], [])
	corrections.weights = [zeros(net.weightsSizes[i]) for i in 1:net.numLayers-1]
	corrections.biases  = [zeros(x) for x in net.layerSizes[2:end]]

	#TODO: Fazer esse loop correr para apenas alguns exemplos de cada vez
	for idx_example in 1:length(trainingData)
		trainingInput = trainingData[idx_example][1]
		correctResult = trainingData[idx_example][2]
		
		#Calcula a correção do resultado desejada por esse conjunto de treino
		desiredCorrections = correctResult - trainingPredict(net, trainingInput)
		
		#Para cada camada, da última pra primeira, calcula as mudanças desejadas por esse conjunto de treino
		for idx_layer in net.numLayers-1:-1:1
			desiredCorrections = applyGradient(net, corrections, idx_layer, desiredCorrections)
		end
		
		numExemplos += 1
	end
	
	#Faz a média das mudanças desejadas
	corrections.weights /= numExemplos
	corrections.biases  /= numExemplos
	
	#Aplica correções na rede
	net.weights += corrections.weights
	net.biases  += corrections.biases
end
#---------------------------------------------------


#---------- Funções chamadas pelo Usuário ----------
#Cria uma nova rede
function createNetwork(layerSizes::Array{Int, 1})
	#----- Error checking -----
	if length(layerSizes) < 2
		println("There should be at least 2 layers!")
		return
	end
	
	for x in layerSizes
		if x < 1
			println("Error! Layers should have sizes greater than zero!")
			return
		end
	end
	#--------------------------
	
	
	#Creates an empty network
	net = Network(layerSizes, 0, [], [], [], [])
	
	#Set sizes variables	
	net.numLayers = length(layerSizes)
	net.weightsSizes = [(layerSizes[i+1], layerSizes[i]) for i in 1:net.numLayers-1]
	
	#Creates the randomized weight matrixes
	net.weights = [randn(net.weightsSizes[i]) / √net.layerSizes[i+1]  for i in 1:net.numLayers-1]
	
	#Creates biases
	net.biases = [zeros(x) for x in layerSizes[2:end]]
	
	return net
end

#Treina uma rede existente
function trainNetwork(net::Network, trainingData::Array{ Tuple{Array{Float64,1}, Array{Float64,1}} , 1})
	#= Training data format
		Tuple com 2 vetores.
		O primeiro é o input inicial da rede
		O segundo é o resultado esperado (correto) que uma rede calibrada encontraria
	=#
	
	data = copy(trainingData)
	batchSize = 200
	
	#Shuffle the training data array
	for c in 1:length(data)
		i = Int(floor(rand() * length(data)) + 1)
		j = Int(floor(rand() * length(data)) + 1)
		
		data[i], data[j] = data[j], data[i]
	end
	
	#Separates the training data in batches and descend through the gradient of each batch
	while length(data) > batchSize
		trainingStep(net, data[1:batchSize])
		splice!(data, 1:batchSize)
	end
	
	#Last batch
	(length(data) > 0) && trainingStep(net, data)
end

#Passes input data through the network and gives the result
function predict(net::Network, inputData::Array{Float64, 1})
	#Error checking
	if length(inputData) != net.layerSizes[1]
		println("Error! Input data should have $(net.layerSizes[1]) elements.")
		return
	end
	
	net.z = []
	
	predition = inputData
	
	#Passes through all layers
	for i in 1:net.numLayers-1
		predition = activation.(net.weights[i] * predition  +  net.biases[i])
	end
		
	return predition
end

#Testa quão bem a rede prevê o resultado de um conjunto de dados
function testNetwork(net::Network, testData::Array{ Tuple{Array{Float64,1}, Array{Float64,1}} , 1})
	numAcertos = 0
	numTestes = length(testData)
	
	for test in testData
		#Guarda qual índice do output deve ser o maior
		answer = (test[2][1] > test[2][2]) ? 1 : 2
		
		#Acha qual índice a rede acha que é o maior
		output = predict(net, test[1])
		prediction = (output[1] > output[2]) ? 1 : 2
		
		#Checa se acertou
		(prediction == answer) && (numAcertos += 1)
	end
	
	fracAcertos = Float64(numAcertos) / numTestes;
	
	println("Acertos: $numAcertos/$numTestes")
	println("$(fracAcertos * 100)% de acertos.")
	
	return fracAcertos
end
#---------------------------------------------------


function list()
	println("createNetwork(layerSizes")
	println("\tReturns a Network object.")
	println("\tlayerSizes: Int array with the size of each layer of the network, including the input and output layer.\n")
	
	
	println("trainNetwork(net, trainingData)")
	println("\tTreina a rede 'net' com o conjunto de dados passado")
	println("\tnet: Target Network object")
	println("\ttrainingData: Dados usados pra treinar a rede. Deve estar no formato Array{ Tuple{Array{Float64,1}, Array{Float64,1}} , 1}
		  , isto é, um conjunto de treino é um tuple cujo primeiro elemento é o vetor de input e o segundo é o output desejado.
		   A função recebe um array desses tuples e realiza o treino para cada um desses tuples.\n")
	
	println("predict(net, inputData)")
	println("\tPassa i input pela rede e retorna o output.\n")
	
	println("testNetwork(net, testData)")
	println("\tTesta quão boa a rede 'net' é em obter o resultado correto. Retorna a taxa de acertos.")
	println("\ttestData deve estar no mesmo formato que trainingData da função 'trainNetwork'\n")
end

end #module
